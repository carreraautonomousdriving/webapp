#!/usr/bin/python
# -*- coding: utf-8 -*-
# Obiges ist elementar wichtig!
from .models import calculations
from .TrackInstruction import TrackInstruction
from .RaceInstructionSet import RaceInstructionSet
import math
import time
import subprocess

# TESTWEISE:
streckenteile = []
streckenteileZweiteRunde = []
#spannungen = []
#pulse = []
frequenzPWM = 1000
zweiteRundeNochNichtAusgelesen = True



# Ich benoetige:
#    - In welchem Abstand sind die Impulse? Damit ich die Anzahl der Impulse berechnen kann.
#       ---> 10ms
#    - Laenge einer kurzen und einer langen Gerade?
#       ---> kurz: 115mm und lang: 345mm
#    - "geschwindigkeitKurve" (bei 6V), "beschleunigung", Bremsbeschleunigung "bremsen" fehlen alle noch
#       ---> Gemessen und unten eingetragen



# Streckenteile aus der Liste "streckenteile" aus Zahl in Name konvertieren
def auslesen(argument):
    switcher = {
        10: "Short_Curve_R",
        11: "Short_Curve_L",
        12: "Long_Curve_R",
        13: "Long_Curve_L",
        14: "Short_Straight",
        15: "Long_Straight",
        16: "Start"
    }
    return switcher.get(argument, "Invalid")


# Gibt an wie viele aufeinanderfolgende Teile kommen
def anzahlGeraden():
    anzahl = 0
    while len(streckenteile) > 0:
        # "Short_Straight":
        if streckenteile[0] == 14:
            anzahl = anzahl + 1
            streckenteile.remove(streckenteile[0])
        # "Long_Straight" und "Start"
        elif streckenteile[0] == 15 or streckenteile[0] == 16:
            anzahl = anzahl + 3
            streckenteile.remove(streckenteile[0])
        else:
            return anzahl
    return anzahl


# Berechnet die Beschleunigungs- und Bremszeit auf der Geraden
def zeitAufGeraden(anzahlGleicherTeile):
    # Die Liste "zeit" besteht aus der Beschleunigungszeit und der Abbremszeit
    zeit = [0, 0]
    # Beschleunigungszeit:
    zeit[0] = round(math.exp(anzahlanzahlGleicherTeile * 0.23104906) * 22.5, 0)
    # geschwindigkeitKurve * ((-1 + math.sqrt(1 - (2 * anzahlGleicherTeile * 11,5 * beschleunigung)/((1 + beschleunigung/bremsen) * geschwindigkeitKurve * geschwindigkeitKurve))) / beschleunigung)
    # Abbremszeit:
    zeit[1] = round((10*anzahlGleicherTeile)/3,0)
    # geschwindigkeitKurve * (beschleunigung/bremsen) * ((-1 + math.sqrt(1 - (2 * anzahlGleicherTeile * 11,5 * beschleunigung)/((1 + beschleunigung/bremsen) * geschwindigkeitKurve * geschwindigkeitKurve))) / beschleunigung)
    return zeit


#Spannungen und Zeiten werden an die Liste angehängt. Zeit in Impulse (je 1ms) und Spannungen in % von 14.8V
def anfuegen(zeitInS, spannungInV):
    #pulse.append(zeitInS)
    #spannungen.append(spannungInV)
    #TODO: An die instructionList die Spannungen und jeweiligen Zeiten hinzufügen
    tsi.addInstruction(TrackInstruction(spannungInV, ZeitInS))


def addStartSequence():
    #Beim Start für 35ms 70% der Spannung (14,8V) anlegen
    tsi.addInstrcution(TrackInstruction(0.7, 35))
    # Das Lichtschrankenpanel und das Startpanel (die ersten beiden Positionen) überspringen, aber für die 2. Runde speichern. PROGRAMMSCHLEIFEN SIND FÜR SCHWÄCHLINGE!
    streckenteileZweiteRunde.append(streckenteile[0])
    streckenteile.remove(streckenteile[0])
    streckenteileZweiteRunde.append(streckenteile[0])
    streckenteile.remove(streckenteile[0])



# TODO: Funktion, die schaut ob nach einer Kurve eine Gerade kommt und ändert dementsprechend pulse[] an den jeweiligen Elementen.
#       Am Ende der Gerade soll das Auto bereits beschleunigen (Kurvenzeit kürzer) und dementsprechend länger bremsen vor der
#       nächsten Kurve (Wahrscheinlich am sinnvollsten, wenn die Kurvenzeit verkürzt wird und die Gerade als länger angesehen wird
#       und dementsprechend auch Beschleunigungs- und Bremszeit mit zeitAufGeraden() berechnet werden.)


# Streckenteile auslesen und an die jeweiligen Listen anfügen
def streckenteileAuslesenAnhaengen(streckenteile, track_id):
    while len(streckenteile) > 0:
        #Streckenteil für zweite Runde saven
        streckenteileZweiteRunde.append(streckenteile[0])
        #eigentliches Auslesen
        if streckenteile[0] == 10:  # "Short_Curve_R":
            #Rechtskurve bei 52% der Spannung (14.8V) für 42.66667ms
            anfuegen(42, 0.52)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 11:  # "Short_Curve_L":
            #Linkskurve bei 46% der Spannung (14.8V) für 36.581ms
            anfuegen(36, 0.46)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 12:  # "Long_Curve_R":
            #Rechtskurve bei 52% der Spannung (14.8V) für 128ms
            anfuegen(128, 0.52)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 13:  # "Long_Curve_L":
            #Linkskurve bei 46% der Spannung (14.8V) für 109,743ms
            anfuegen(109, 0.46)
            streckenteile.remove(streckenteile[0])
        #"Short_Straight" OR "Long_Straight" OR "Start". Alle hintereinanderhängenden Geraden werden in einem Rutsch ausgelesen
        elif streckenteile[0] == 14 or streckenteile[0] == 15 or streckenteile[0] == 16:
            zeit = zeitAufGeraden(anzahlGeraden())
            # Bei 14,8V:
            anzahlPulsSignale = zeit[0] #Beschleunigungszeit in ms
            anfuegen(anzahlPulsSignale, 0.7)
            # Bei 0V:
            anzahlPulsSignale = zeit[1] #Bremszeit in ms
            anfuegen(anzahlPulsSignale, -1)
            streckenteile.remove(streckenteile[0])
        else:
            #Falls eine falsche Zahl reingerutscht ist.
            streckenteile.remove(streckenteile[0])



#Umrechnung in Listen mit Zeit und Spannung
def main_calculation(streckenteile, track_id, rundenzahl):
    tsi = RaceInstructionSet(rundenzahl)
    for teil in streckenteile:
        print(str(teil) + ' ' + auslesen(teil))
        #print("%s %s" % (teil, auslesen(teil)))

    #TODO: Startingsequence greift noch nicht richtig auf die len(Liste) zu
    if len(tsi) == 0:
        #Start des Autos initiieren
        addStartSequence()

    # Hauptfunktion aufrufen, die die Streckenteile aus der Liste ausliest und in Spannungen und Impulse überträgt
    streckenteileAuslesenAnhaengen(streckenteile, track_id)

    # Abfrage, dass nach dem ersten Durchlaufen die 2. Runde initialisiert wird und danach erst alles in die DB speichern.
    if zweiteRundeNochNichtAusgelesen:
        zweiteRundeNochNichtAusgelesen = False
        # Jetzt beginnt die zweite Runde
        tsi.ResetIndex = len(tsi)
        streckenteileAuslesenAnhaengen(streckenteileZweiteRunde, track_id)

    # Save track calculation
    tsi.serialize("/opt/arerrac/currentTrack.bin")
    time.sleep(0.2)
    subprocess.Popen(["systemctl", "restart", "arerrac-track"])
    return True


def saveCalculationsToDB(track_id):
    i = 0
    while (i < len(pulse)):
        c = calculations(track_id=track_id, calculation_position=i,
                         voltage=spannungen[i], time=pulse[i])
        c.save()
        i += 1

# TODO:
#
# Kurven:
#    - Beschleunigung aus der Kurve heraus???
#
# Geraden:
#    - Beim Start wird eine Liste an Befehlen (Ohne Lichtschrankenpanel und Startpanel) ausgegeben
#    - Vor der Zielgeraden (Beginnt mit Lichtschrankenpanel und Startpanel) muss immer eine Kurve sein
#    - Beim Durchfahren der Lichtschranke soll eine andere Liste an Befehlen (mit Lichtschrankenpanel und Startpanel) ausgegeben
#      werden, die ab dann für jede Runde verwendet wird.
#    - Nach einer gewissen Anzahl an durchläufen wird die Liste nicht mehr ausgegeben
#       ---> TODO: Rundenzahl wid noch eingegeben und hier verarbeitet. Wie auch immer...