from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import logging

from .models import *
from .utils import *


logger = logging.getLogger(__name__)


def index(request):
    return render(request, 'race/index.html')


def raceconfig(request):
    latest_track_list = track.objects.order_by('-track_date')[:5]
    context = {'latest_track_list': latest_track_list, }
    return render(request, 'race/raceConfiguration.html', context)


def griddesign(request):
    return render(request, 'race/gridDesign.html')


# def detail(request, track_id):
#     track = get_object_or_404(track, pk=track_id)
#     return render(request, 'race/detail.html', {'question': question})


@csrf_exempt
def createTrack(request):
    if request.method == 'POST':
        track_part_index = request.POST['track_part_index']
        track_part_type = request.POST['track_part_type']

        trackId = track.objects.latest('id')

        tb = trackbuild(track_id=trackId, track_part_index=track_part_index,
                        track_part_type_id=track_part.objects.get(pk=track_part_type))
        tb.save()
        return HttpResponse("Track successfully created!")


@csrf_exempt
def initTrack(request):
    if request.method == 'POST':
        track_name = request.POST['track_name']
        t = track(track_name=track_name)
        t.save()
    return HttpResponse("Track successfully initialized!")


@csrf_exempt
def calculateTrack(request, id):
    sortedList = []
    rounds = request.POST['roundCount']

    sortedTrackparts = trackbuild.objects.filter(track_id=id).order_by(
        'track_part_index')

    for part in sortedTrackparts:
        tmp = getattr(part, 'track_part_type_id')
        sortedList.append(getattr(tmp, 'id'))

    main_calculation(sortedList, id, rounds)
    return render(request, 'race/index.html')
