from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('raceconfiguration/', views.raceconfig, name='raceconfig'),
    path('griddesign/', views.griddesign, name='griddesign'),
    path('griddesign/initTrack/', views.initTrack, name='inittrack'),
    path('griddesign/saveTrack/', views.createTrack, name='createtrack'),
    path('raceconfiguration/calculateTrack/<int:id>/',
         views.calculateTrack, name='calculatetrack'),
]
