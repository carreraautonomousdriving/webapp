# Generated by Django 3.0.6 on 2020-05-23 17:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('race', '0007_auto_20200523_1616'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trackbuild',
            old_name='track_part',
            new_name='track_part_type',
        ),
    ]
