# Generated by Django 3.0.6 on 2020-05-23 17:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('race', '0008_auto_20200523_1945'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trackbuild',
            old_name='track_name',
            new_name='track_id',
        ),
        migrations.RenameField(
            model_name='trackbuild',
            old_name='track_part_type',
            new_name='track_part_type_id',
        ),
    ]
