'use strict';

// settings
let showCanvasLines = true;
let showCanvasImages = true;
var StartBeginningPointX = 50;
var StartBeginningPointY = 50;
let ImageWidth = 100;
let ImageHeight = 100;

//GlobalVariables
var NextBeginningPointX = StartBeginningPointX;
var NextBeginningPointY = StartBeginningPointY;
var listTracks = new Array();

/* User Buttons */
// function rotateLeftTarget() {
// 	if (listTracks === undefined || listTracks === null || listTracks.length === 0) return;

// 	var last_element = listTracks[listTracks.length - 1];
// 	commandPattern.executeCommands('rotateLeft', last_element);

// 	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', last_element);
// 	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', last_element);

// 	drawContext();
// }

// function rotateRightTarget() {
// 	if (listTracks === undefined || listTracks === null || listTracks.length === 0) return;

// 	var last_element = listTracks[listTracks.length - 1];
// 	commandPattern.executeCommands('rotateRight', last_element);

// 	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', last_element);
// 	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', last_element);

// 	drawContext();
// }

/* JS Backend */
var commandPattern = (function() {
	var commandSet = {
		rotateLeft: function(trackPartType) {
			trackPartType.rotated += 30;
			trackPartType.rotated %= 360;
			return 'This is argument 1 ' + arg1;
		},
		rotateRight: function(trackPartType) {
			trackPartType.rotated -= 30;
			trackPartType.rotated %= 360;
		},
		getLineBeginX: function(trackPartType) {
			return trackPartType.LineBeginX;
		},
		getLineBeginY: function(trackPartType) {
			return trackPartType.LineBeginY;
		},
		getLineEndX: function(trackPartType) {
			//return trackPartType.LineBeginX + trackPartType.length * Math.cos(trackPartType.getWholeRotationRadian());
			return (
				trackPartType.LineBeginX +
				trackPartType.length * Math.cos(commandPattern.executeCommands('getWholeRotationRadian', trackPartType))
			);
		},
		getLineEndY: function(trackPartType) {
			//return trackPartType.LineBeginY + trackPartType.length * Math.sin(trackPartType.getWholeRotationRadian());
			return (
				trackPartType.LineBeginY +
				trackPartType.length * Math.sin(commandPattern.executeCommands('getWholeRotationRadian', trackPartType))
			);
		},
		getRotationRadian: function(trackPartType) {
			return trackPartType.rotated * Math.PI / 180;
		},
		getBeginRotation: function(trackPartType) {
			return trackPartType.rotated;
		},
		setBeginRotation: function(trackPartType, value) {
			trackPartType.rotated = value;
		},
		getEndRotation: function(trackPartType) {
			return trackPartType.rotated + trackPartType.standardRotation;
		},
		setEndRotation: function(trackPartType, value) {
			trackPartType.rotated = Math.abs(trackPartType.standardRotation - value) % 360;
		},
		getWholeRotation: function(trackPartType) {
			return trackPartType.standardRotation + trackPartType.rotated;
		},
		getWholeRotationRadian: function(trackPartType) {
			return (trackPartType.standardRotation + trackPartType.rotated) * Math.PI / 180;
		},
		DoPointsDiffer: function(Point1X, Point1Y, Point2X, Point2Y) {
			return Math.abs(Point1X - Point2X) > 0.1 || Math.abs(Point1Y - Point2Y) > 0.1;
		},

		executeCommands: function(name) {
			return commandSet[name] && commandSet[name].apply(commandSet, [].slice.call(arguments, 1)); //gives arguments list
		}
	};
	return commandSet;
})();

function removeTarget() {
	if (listTracks === undefined || listTracks === null || listTracks.length === 0) return;

	listTracks.splice(listTracks.length - 1, 1);

	if (listTracks.length !== 0) {
		var last_element = listTracks[listTracks.length - 1];
		NextBeginningPointX = commandPattern.executeCommands('getLineEndX', last_element);
		NextBeginningPointY = commandPattern.executeCommands('getLineEndY', last_element);
	} else {
		NextBeginningPointX = StartBeginningPointX;
		NextBeginningPointY = StartBeginningPointY;
	}

	drawContext();
}

function validateTarget() {
	let textElement = document.getElementById('infoText');

	if (listTracks === undefined || listTracks === null || listTracks.length === 0) {
		textElement.innerText = 'Multiple StartTracks detected!';
		return;
	}

	console.log('GetLineBeginX ' + commandPattern.executeCommands('getLineBeginX', listTracks[0]));
	console.log('GetLineBeginY' + commandPattern.executeCommands('getLineBeginY', listTracks[0]));
	console.log('getLineEndX ' + commandPattern.executeCommands('getLineEndX', listTracks[listTracks.length - 1]));
	console.log('GetLineEndY' + commandPattern.executeCommands('getLineEndY', listTracks[listTracks.length - 1]));

	// console.log('GetLineBeginY' + listTracks[0].getLineBeginY());
	// console.log('getLineEndX' + .getLineEndX());
	// console.log('GetLineEndY' + .getLineEndY());

	/*
  Dismiss Float Value.. (Solution was 49,999 & 50 before) 
  */
	if (
		commandPattern.executeCommands(
			'DoPointsDiffer',
			commandPattern.executeCommands('getLineBeginX', listTracks[0]),
			commandPattern.executeCommands('getLineBeginY', listTracks[0]),
			commandPattern.executeCommands('getLineEndX', listTracks[listTracks.length - 1]),
			commandPattern.executeCommands('getLineEndY', listTracks[listTracks.length - 1])
		)
	) {
		textElement.innerText = 'Track not properly closed!';
		return false;
	}

	let startTracksCounter = 0;
	listTracks.forEach((trackpart) => {
		if (trackpart instanceof StartTrackPartType) {
			startTracksCounter++;
		}
	});

	if (startTracksCounter !== 1) {
		textElement.innerText = 'None or Multiple StartTracks detected!';
		return false;
	}

	textElement.innerText = 'Validation Passed!';
	return true;
}

function sendTarget() {
	let textElement = document.getElementById('infoText');
	if (validateTarget()) {
		textElement.innerText = 'Validation Passed - Track sent!';
		saveTrack();
		return true;
	} else {
		textElement.innerText = 'Validation NOT Passed - Track not sent!';
		return false;
	}
}

/* Canvas Move Buttons */
function CanvasContentUp() {
	listTracks.forEach((trackpart) => {
		trackpart.LineBeginY -= 20;
	});
	NextBeginningPointY -= 20;
	drawContext();
}

function CanvasContentDown() {
	listTracks.forEach((trackpart) => {
		trackpart.LineBeginY += 20;
	});
	NextBeginningPointY += 20;
	drawContext();
}

function CanvasContentLeft() {
	listTracks.forEach((trackpart) => {
		trackpart.LineBeginX -= 20;
	});
	NextBeginningPointX -= 20;
	drawContext();
}

function CanvasContentRight() {
	listTracks.forEach((trackpart) => {
		trackpart.LineBeginX += 20;
	});
	NextBeginningPointX += 20;
	drawContext();
}

/* Toolbar Buttons */
function drawShortCurveLeft() {
	let StraightLength = 33;
	let standardRotation = 30;
	let rotated = 0;

	let track = new ShortCurveLeftTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

function drawShortCurveRight() {
	let StraightLength = 33;
	let standardRotation = -30;
	let rotated = 0;

	let track = new ShortCurveRightTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

function drawMiddleCurveLeft() {
	let StraightLength = 66;
	let standardRotation = 60;
	let rotated = 0;

	let track = new MiddleCurveLeftTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

function drawMiddleCurveRight() {
	let StraightLength = 66;
	let standardRotation = -60;
	let rotated = 0;

	let track = new MiddleCurveRightTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

function drawStraight() {
	let StraightLength = 100;
	let standardRotation = 0;
	let rotated = 0;

	let track = new StraightTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

function drawShortStraight() {
	let StraightLength = 100 / 3;
	let standardRotation = 0;
	let rotated = 0;

	let track = new ShortStraightTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

function drawStart() {
	let StraightLength = 100;
	let standardRotation = 0;
	let rotated = 0;

	let track = new StartTrackPartType(
		NextBeginningPointX,
		NextBeginningPointY,
		StraightLength,
		standardRotation,
		rotated
	);
	AutoRotation(track);

	NextBeginningPointX = commandPattern.executeCommands('getLineEndX', track);
	NextBeginningPointY = commandPattern.executeCommands('getLineEndY', track);

	listTracks.push(track);
	drawContext();
}

/* Canvas Checkbox Buttons */
function showLine() {
	let ShowLineCheckBox = document.getElementById('ShowLineCheckBox');
	showCanvasLines = ShowLineCheckBox.checked;
	drawContext();
}

function showImage() {
	let ShowImageCheckBox = document.getElementById('ShowImageCheckBox');
	showCanvasImages = ShowImageCheckBox.checked;
	drawContext();
}

function removeAll() {
	listTracks = [];
	drawContext();
}

function initButton() {
	/* User Buttons */
	// let rotateLeftButton = document.getElementById("rotateLeftButton");
	// rotateLeftButton.addEventListener('click', rotateLeftTarget);

	// let rotateRightButton = document.getElementById("rotateRightButton");
	// rotateRightButton.addEventListener('click', rotateRightTarget);

	let removeButton = document.getElementById('removeButton');
	removeButton.addEventListener('click', removeTarget);

	let removeAllButton = document.getElementById('removeAllButton');
	removeAllButton.addEventListener('click', removeAll);

	let validateButton = document.getElementById('validateButton');
	validateButton.addEventListener('click', validateTarget);

	let sendButton = document.getElementById('sendButton');
	sendButton.addEventListener('click', sendTarget);

	/* Canvas Move Buttons */
	let CanvasContentUpButton = document.getElementById('CanvasContentUpButton');
	CanvasContentUpButton.addEventListener('click', CanvasContentUp);

	let CanvasContentDownButton = document.getElementById('CanvasContentDownButton');
	CanvasContentDownButton.addEventListener('click', CanvasContentDown);

	let CanvasContentLeftButton = document.getElementById('CanvasContentLeftButton');
	CanvasContentLeftButton.addEventListener('click', CanvasContentLeft);

	let CanvasContentRightButton = document.getElementById('CanvasContentRightButton');
	CanvasContentRightButton.addEventListener('click', CanvasContentRight);

	/* Canvas Checkbox Buttons */

	let ShowLineCheckBox = document.getElementById('ShowLineCheckBox');
	ShowLineCheckBox.addEventListener('click', showLine);

	let ShowImageCheckBox = document.getElementById('ShowImageCheckBox');
	ShowImageCheckBox.addEventListener('click', showImage);

	/*Track Buttons*/

	let drawShortCurveLeftButton = document.getElementById('drawShortCurveLeftButton');
	drawShortCurveLeftButton.addEventListener('click', drawShortCurveLeft);

	let drawShortCurveRightButton = document.getElementById('drawShortCurveRightButton');
	drawShortCurveRightButton.addEventListener('click', drawShortCurveRight);

	let drawMiddleCurveLeftButton = document.getElementById('drawMiddleCurveLeftButton');
	drawMiddleCurveLeftButton.addEventListener('click', drawMiddleCurveLeft);

	let drawMiddleCurveRightButton = document.getElementById('drawMiddleCurveRightButton');
	drawMiddleCurveRightButton.addEventListener('click', drawMiddleCurveRight);

	let drawStraightButton = document.getElementById('drawStraightButton');
	drawStraightButton.addEventListener('click', drawStraight);

	let drawShortStraightButton = document.getElementById('drawShortStraightButton');
	drawShortStraightButton.addEventListener('click', drawShortStraight);

	let drawStartButton = document.getElementById('drawStartButton');
	drawStartButton.addEventListener('click', drawStart);
}

/* init */

function init() {
	/****** INIT BUTTONS ******/
	initButton();
}

/* Type Classes */
class TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		this.LineBeginX = LineBeginX;
		this.LineBeginY = LineBeginY;
		this.length = length;

		if (standardRotation !== undefined || standardRotation !== null) this.standardRotation = standardRotation;
		else this.standardRotation = 0;

		if (rotated !== undefined || rotated !== null) this.rotated = rotated;
		else this.rotated = 0;
	}
}

class ShortCurveLeftTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);

		this.entryAngle = 0;
		this.exitAngle = 30;
	}
	getTrackPart() {
		return 11;
	}
}

class ShortCurveRightTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);
	}
	getTrackPart() {
		return 10;
	}
}

class MiddleCurveLeftTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);
	}
	getTrackPart() {
		return 13;
	}
}

class MiddleCurveRightTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);
	}
	getTrackPart() {
		return 12;
	}
}

class StartTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);
	}
	getTrackPart() {
		return 16;
	}
}

class StraightTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);
	}
	getTrackPart() {
		return 15;
	}
}

class ShortStraightTrackPartType extends TrackPartType {
	constructor(LineBeginX, LineBeginY, length, standardRotation, rotated) {
		super(LineBeginX, LineBeginY, length, standardRotation, rotated);
	}
	getTrackPart() {
		return 14;
	}
}

function saveTrack() {
	var counter = 0;
	var savecounter = listTracks.length - 1;
	var nameBox = document.getElementById('trackNameBox');
	let textElement = document.getElementById('infoText');

	if (nameBox.value === '') {
		textElement.innerText = "Didn't you forget something?";
		return;
	}

	SetStartTrackPositionToEnd();

	$.ajax({
		contentType: 'application/x-www-form-urlencoded',
		url: 'initTrack/',
		data: {
			csrfmiddlewaretoken: '{{ csrf_token }}',
			track_name: nameBox.value
		},
		type: 'POST'
	}).done(function() {
		for (var i = 0; i < listTracks.length; i++) {
			$.ajax({
				contentType: 'application/x-www-form-urlencoded',
				url: 'saveTrack/',
				data: {
					csrfmiddlewaretoken: '{{ csrf_token }}',
					track_part_index: counter++,
					track_part_type: listTracks[i].getTrackPart()
				},
				type: 'POST'
			}).done(function() {
				savecounter--;
				if (savecounter === 0) {
					$.ajax({
						contentType: 'application/x-www-form-urlencoded',
						url: 'calculateTrack/',
						type: 'POST'
					}).done(function(response) {});
				}
			});
		}
	});
}

function AutoRotation(trackPartType) {
	if (trackPartType === undefined || trackPartType === null || listTracks.length <= 0) return;

	var last_element = listTracks[listTracks.length - 1];

	//trackPartType.setBeginRotation(last_element.getEndRotation());
	commandPattern.executeCommands(
		'setBeginRotation',
		trackPartType,
		commandPattern.executeCommands('getEndRotation', last_element)
	);
}

function drawContext() {
	let canvasElement = document.getElementById('TrackCanvas');
	let ctx = canvasElement.getContext('2d');

	ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);

	if (listTracks.length > 0) {
		listTracks.forEach((trackpart) => {
			if (trackpart instanceof StraightTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/straight.png');
				}
				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'red');
				}
			} else if (trackpart instanceof ShortStraightTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/shortstraight.png');
				}

				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'red');
				}
			} else if (trackpart instanceof StartTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/start.png');
				}

				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'green');
				}
			} else if (trackpart instanceof ShortCurveLeftTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/shortCurve_l.png');
				}

				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'red');
				}
			} else if (trackpart instanceof ShortCurveRightTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/shortCurve_r.png');
				}

				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'red');
				}
			} else if (trackpart instanceof MiddleCurveLeftTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/middleCurve_l.png');
				}

				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'red');
				}
			} else if (trackpart instanceof MiddleCurveRightTrackPartType) {
				if (showCanvasImages) {
					DrawCanvasImages(ctx, trackpart, '/static/race/trackpics/middleCurve_r.png');
				}

				if (showCanvasLines) {
					DrawCanvasLines(ctx, trackpart, 'red');
				}
			}
		});
	}
}

function DrawCanvasImages(ctx, trackpart, pathOfImage) {
	draw(
		ctx,
		pathOfImage,
		commandPattern.executeCommands('getLineBeginX', trackpart),
		commandPattern.executeCommands('getLineBeginY', trackpart),
		ImageWidth,
		ImageHeight,
		commandPattern.executeCommands('getRotationRadian', trackpart)
	);
}

function DrawCanvasLines(ctx, trackpart, color) {
	ctx.beginPath();
	ctx.moveTo(
		commandPattern.executeCommands('getLineBeginX', trackpart),
		commandPattern.executeCommands('getLineBeginY', trackpart)
	);
	ctx.lineTo(
		commandPattern.executeCommands('getLineEndX', trackpart),
		commandPattern.executeCommands('getLineEndY', trackpart)
	);
	ctx.strokeStyle = color;
	ctx.stroke();
}

// --------------------------------------------------
// Wrap drawing operation in a method
// Source: https://www.youtube.com/watch?v=RbLvOiW0HvU
// --------------------------------------------------
function draw(ctx, imageSrc, X, Y, width, length, rotation) {
	var image = new Image();
	image.src = imageSrc;
	// If the image is not ready, wait and try again in
	// approx 50 milliseconds
	if (!image.complete) {
		setTimeout(function() {
			draw(ctx, imageSrc, X, Y, width, length, rotation);
		}, 50);
		return;
	}

	// Basic image draw
	if (rotation === undefined || rotation === null || rotation === 0) ctx.drawImage(image, X, Y, width, length);
	else drawRotatedImage(ctx, image, X, Y, width, length, rotation);
}

// --------------------------------------------------
// Generic method to draw an image rotated on its
// midpoint.
// --------------------------------------------------
function drawRotatedImage(ctx, image, x, y, width, height, rotation) {
	// Cache calculation for half width and height
	var halfWidth = width / 2;
	var halfHeight = height / 2;

	// Save canvas context state
	ctx.save();

	// Input transformation: translate to midpoint of image
	ctx.translate(x + halfWidth, y + halfHeight);

	// Input transformation: rotate by desired rotation
	ctx.rotate(rotation);

	// Draw the image
	ctx.drawImage(image, -halfWidth, -halfHeight, width, height);

	// Restore previous context state
	ctx.restore();
}

function SetStartTrackPositionToEnd() {
	var positionBefore = -1;

	//Get StartTrack Position
	for (var i = 0; i < listTracks.length; i++) {
		if (listTracks[i] instanceof StartTrackPartType) {
			positionBefore = i;
		}
	}

	//Check
	if (positionBefore < 0) return;

	var tempList = new Array();

	//Save behind old StartTrack Position to new List
	for (var i = positionBefore + 1; i < listTracks.length; i++) {
		tempList.push(listTracks[i]);
	}

	//Save before, including, old StartTrack Position to new List
	for (var i = 0; i <= positionBefore; i++) {
		tempList.push(listTracks[i]);
	}

	listTracks = tempList;
}

document.addEventListener('DOMContentLoaded', init);
