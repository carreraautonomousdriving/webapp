function startRace(buttonID) {
	var trackID = buttonID;
	var roundCount = document.getElementById('roundCounts');
	var difficulty = document.getElementById('difficulty');
	$.ajax({
		contentType: 'application/x-www-form-urlencoded',
		url: 'calculateTrack/' + trackID + '/',
		data: {
			roundCount: roundCount.value,
			difficulty: difficulty.options[difficulty.selectedIndex].value
		},
		type: 'POST'
	}).done(function(response) {
		console.log(response);
	});
}
