from django.contrib import admin
from .models import *

admin.site.register(track)
admin.site.register(track_part)
admin.site.register(trackbuild)
admin.site.register(calculations)


# class CourseAdmin(admin.ModelAdmin):
#     list_display = ('track_name', 'track_date', 'track_image',)
#     fieldsets = (
#         (None, {
#             fields=(
#                 'track_name',
#                 'track_date',
#                 'track_image',
#             )
#         })
#     )

#admin.site.register(Course, CourseAdmin)
