from django.db import models
import datetime

# Create your models here.


class track(models.Model):
    track_name = models.CharField(max_length=200)
    track_date = models.DateTimeField(
        'date published', default=datetime.datetime.now())
    track_image = models.CharField(
        max_length=300, default='C:\\Users\\leong\\Documents\\Testdemo\\Testproject\\templates')


class track_part(models.Model):
    track_part_name = models.CharField(max_length=100)


class trackbuild(models.Model):
    track_id = models.ForeignKey(track, on_delete=models.PROTECT)
    track_part_index = models.IntegerField()
    track_part_type_id = models.ForeignKey(
        track_part, on_delete=models.PROTECT)


class calculations(models.Model):
    track_id = models.ForeignKey(track, on_delete=models.PROTECT)
    calculation_position = models.IntegerField()
    voltage = models.FloatField()
    time = models.FloatField()
